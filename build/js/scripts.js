var didScroll;
var lastScrollTop = 0;
var delta = 5; // how far to scroll before hiding nav
var navbarHeight = $('#carousel').outerHeight();
var myScroll; // used for mobile tabs

// --------------------------------------------------------------
// Use this function to reset the purchase process progress bar
// --------------------------------------------------------------
function resetProgressBar() {
    // this spaces out the progress indicator labels
    var numElements = $(".progress-indicator li").length - 1;
    var ratio = 100 / numElements;
    var count = 0;
    //console.log(numElements, ratio);
    setTimeout(function() {
        $(".progress-indicator li").each(function() {
            $(this).css("left", count + "%")
                .css("margin-left", $(this).width() / 2 * -1);
            count += ratio;
        });
    }, 20);
}

// --------------------------------------------------------------
// Use this function to advance the purchase process progress bar
// --------------------------------------------------------------
function progressBarAdvance() {
    var numElements = $(".progress-indicator li").length - 1;
    var ratio = Math.floor(100 / numElements);
    console.log(ratio);
    var curr = $(".progress-indicator li.current").index();
    $(".progress-indicator li.current").attr("class", "done"); // set current to done
    $(".progress-indicator li").eq(++curr).attr("class", "current"); // set next step to current
    $(".progress-line").attr("class", "progress-line progress-" + (ratio * curr));
}

// --------------------------------------------------------------
//  Sets the row to the same height not all blocks in the container
// --------------------------------------------------------------
var equalheight = function (container) {

	var currentTallest = 0,
	currentRowStart = 0,
	rowDivs = new Array(),
	$el,
	topPosition = 0;

	$(container).each(function () {
		$el = $(this);
		$($el).height('auto');
		topPostion = $el.offset().top;
		//topPostion = $el.position().top;

		if (currentRowStart !== topPostion) {
			for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
};

// --------------------------------------------------------------
// Sets all elements with the same classname to the height of the largest one
// --------------------------------------------------------------
var setSameHeightAll = function(item) {
    var theseItems = $(item);
    var maxHeight = 0;
    $(theseItems).css("height", "auto");
    theseItems.each(function() {
        maxHeight = Math.max(maxHeight, $(this).outerHeight());
    });
    theseItems.css({ height: maxHeight + 'px' });

}
// --------------------------------------------------------------
// Sets all elements with the same classname to the height of the largest one PLUS additional margin for bottom:0 buttons
// --------------------------------------------------------------
var setSameHeightFaq = function(item) {
    var theseItems = $(item);
    var maxHeight = 0;
    var paddedHeight = 50;
    $(theseItems).css("height", "auto");
    theseItems.each(function() {
        maxHeight = Math.max(maxHeight, $(this).outerHeight());
    });
    theseItems.css({ height: (maxHeight + paddedHeight) + 'px' });

}
// --------------------------------------------------------------
// used to check if page has scrolled so we can show/hide the navbar
// --------------------------------------------------------------
function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;
    if ($('.dropdown.open').length > 0)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('.navbar-fixed-top').addClass('nav-up');
        $('.sticky-tabs').addClass('nav-up');
        $('.sticky-tabs').css("top", 0);
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('.navbar-fixed-top').removeClass('nav-up');
            $('.sticky-tabs').removeClass('nav-up');

            if ($('.sticky-tabs').hasClass('affix')) {
                $('.sticky-tabs').css("top", $('.navbar-fixed-top').outerHeight());
            }
        }
    }

    lastScrollTop = st;
}
// --------------------------------------------------------------
// Used to load T&C content
//  _identifier is the name of a HTML file as well as the ID of the element on the page
// --------------------------------------------------------------
function loadTC(_identifier) {
    //console.log( _identifier, _identifier );

    if ($(".panel-body .content img", "#" + _identifier).length) {

        $(".panel-body .content", "#" + _identifier).load("terms-conditions/" + _identifier + ".html", function(responseTxt, statusTxt, xhr) {
            // if(statusTxt == "success") {} // for in case we wanted to do something on success
            if (statusTxt == "error") {
                alert("Error: " + xhr.status + ": " + xhr.statusText + "\nThe content was not found in folder \"terms-conditions\"");
            }
        });
        // scroll to the element
        scrollTo(_identifier);
    }
}
// --------------------------------------------------------------
// Used by the T&C page to scroll to an opened element
// --------------------------------------------------------------
function scrollTo(_identifier) {
    //console.log( $(".panel-body .content", "#" + _identifier) );
    $("html,body").stop().animate({ scrollTop: $(".panel-body .content", "#" + _identifier).offset().top - 150 }, 1000, 'swing', function() {
        //window.location.hash = _identifier;
        var el = document.getElementById(_identifier);
        var id = el.id;
        el.removeAttribute('id');
        location.hash = _identifier;
        el.setAttribute('id', id);
    });
}

// --------------------------------------------------------------
// Toggle checkboxes for ease of use
// --------------------------------------------------------------
function toggle_checkboxes() {
    setTimeout(function() {
        $('input[type=checkbox]', 'table tbody').each(function() {
            //console.log(this);
            $(this).prop('checked', $('#check_all_toggle').prop('checked'));
        });
    }, 100);

    // var inputs = document.getElementsByTagName("input");
    // for(var x=0; x < inputs.length; x++) {
    //     if (inputs[x].type == 'checkbox'){
    //         inputs[x].checked = !inputs[x].checked;
    //     }
    // }
}

function invertTabSelect(num) {
	if ($("#tabSelect").length && num != undefined) {
		setTimeout(function () {
			$('#tabSelect>option').each(function () {
				if ($(this).index() === num){
					$(this).attr('selected', true)
					$("#tabSelect").val($(this).attr('value'));
				} else {
					$(this).attr('selected', false)
				};
			});
		}, 10);
	};
};

function initVerticalTabs() {
    if ($('.vertical-tabs').length) {
        $('.vertical-tabs .tab-content #tab-0 a').each(function() {
            $(this).click(function(e) {
                e.preventDefault();
                let i = $(this).parent().parent().index();
				$('.nav-tabs li:eq(' + i + ') a').tab('show');
				invertTabSelect(i);
            });
        })
    }
}

// Validates between VISA and MASTERCARD
function cardnumber(_trgt) {
    var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
    let card = "";

    if (visaRegEx.test(_trgt.value)) {
        card = 'visa';
    } else if (mastercardRegEx.test(_trgt.value)) {
        card = 'mastercard';
    }

    // set select dropdown to card type
    document.getElementById('card_select').value = card;

}

$(window).load(function () {
	$(window).resize();
});

// --------------------------------------------------------------
// Document is ready, let's set the stage
// --------------------------------------------------------------
$(document).ready(function() {
    // Close mega nav
    $('.close-mega-menu').click(function() {
		$(this).parents('.dropdown').find('a.dropdown-toggle').dropdown('toggle');
    });
    // Init carousel
    $('.carousel').carousel({
        interval: 7000,
        pause: null
    });
    $('.carousel-indicators li:first-child').addClass("active");
    $('.click-to-advance').click(function() {
        $("html, body").animate({ scrollTop: $('#why-us').offset().top }, 700);
    })

    // Additional class toggle for the menu button
    $('.navbar-toggle').click(function() {
        $('.navbar').toggleClass('in');
		$('body').toggleClass('hide-overflow');
    });

    // This is test code, can be ignored *I think*
    $("#navbar_register_btn").on("click", function(e) {
        e.preventDefault();
        $('#ajax_register_account').modal('show');
    })

    // Touch enabled horisontal scrollers for tabs
    if ($('.scroll').length > 0) {
        myScroll = new IScroll('.scroll', {
            scrollX: true,
            scrollY: false,
            mouseWheel: false,
            click: true
        });
    }
    if ($('.scroll-table').length > 0) {
        myScroll = new IScroll('.scroll-table', {
            eventPassthrough: true,
            scrollX: true,
            scrollY: false,
            preventDefault: false
        });
    }

    // Custom code for LTE select box and options
    $("#lte-select").change(function(trgt) {
        //console.log();
        let sel = trgt.target.selectedIndex;
        $('.compare-list li').removeClass('active');
        $('.compare-list li').eq(sel).addClass('active');
    });

    // make the copmare-list LIs also clickable
    $('.compare-list li').click(function() {
        let sel = $(this).index();
        $('.compare-list li').removeClass('active');
        $(this).addClass('active');
        $("#lte-select option").eq(sel).prop('selected', true);
    });

    // Custom code for enquiry select boxes to show more details
    $(".enquiry-select").change(function(trgt) {
        //console.log("changed");
        let sel = trgt.target.selectedIndex;
        let glossary = $("option", this).eq(sel).attr('data-glossary');
        //console.log(sel,glossary);
        $(this).closest(".price-and-feature").children(".selection-definition").html(glossary);
    });

    // Activate tabs
    $('.content-tabs').tab();

    // Use the accordian callback to load its content
    $('.tc-page .panel-group').on('shown.bs.collapse', function(e) {
        let trgt = $(e.target).attr("id");
        loadTC(trgt);
    })
    $('.faq-item-page .panel-group').on('shown.bs.collapse', function(e) {
        let trgt = $(e.target).attr("id");
        scrollTo(trgt);
    })

    // Callback for hiding the overflow on the body
    //$('.dropdown').on('show.bs.dropdown', function() {
        // do something…
    //    $('body').addClass('hide-overflow');
    //})
    //$('.dropdown').on('hide.bs.dropdown', function() {
        // do something…
    //    $('body').removeClass('hide-overflow');
    //})

    // If window has a hash and we're on the T-C page, let's scroll to content
    if (window.location.hash && ($('.tc-page').length > 0 || $('.faq-item-page').length > 0)) {
        let hash = window.location.hash.substring(1); // get hash value
        $(".panel-group").find("[data-target='#" + hash + "']").trigger('click'); // trigger a click on the concertina
        // Now scroll the user down the page to the content        
        setTimeout(function() {
            scrollTo(hash);
        }, 100);
    }
    // Youtube embed
    if ($(".about-ignite-video").length) {
        $(".about-ignite-video").fitVids();
    }

	// Youtube - background Videos
	$('.player-section').closest('body').find('.player').each(function () {
		var src = $(this).attr('data-video-id');
		var startat = $(this).attr('data-start-at');
		var stopat = $(this).attr('data-stop-at');
		var autoPlay = $(this).attr('data-auto-play');
		var mute = $(this).attr('data-mute');
		var volume = $(this).attr('data-volume');
		$(this).attr('data-property', "{videoURL:'" + src + "',containment:'self',autoPlay:" + autoPlay + ", mute:" + mute + ", startAt:" + startat + ", stopAt:" + stopat + ", opacity:1, vol:" + volume + ", showControls:false}");
	});

	if ($('.player').length) {
		$('.player').each(function () {
			if ($(window).width() <= 1024) {
				$(this).hide();
				$('.loading').hide();
			} else {
				var player = $(this);
				player.YTPlayer({
					onReady: function () {
						player.css('opacity', '1');
						player.parent().find('.loading').hide();
						player.removeClass('loading-vid').addClass('ytpVid');
					}
				});
			};
		});
		$('.carousel').on('slid.bs.carousel', function () {
			if ($(window).width() >= 1024) {
				$('.player-section').find('.player').each(function () {
					if (!$(this).hasClass('loading-vid')) {
						if ($(this).parent().hasClass('active')) {
							var slidePlayer = $(this);
							setTimeout(function () {
								slidePlayer.YTPPlay();
								slidePlayer.YTPSetAlign('middle');
							}, 100)
						} else {
							$(this).YTPPause();
						};
					} else {
						$('.loading').hide();
					};
				});
			} else if ($('.player.ytpVid').length) {
				$('.player.ytpVid').YTPStop();
			};
		});
	};
	
    // TODO REMOVE THIS IN PRODUCTION ----------------------------------------
    // TEMP CODE FOR THE SAMPLE WIZARD PAGE > CAN BE REMOVED AT PRODUCTION
    if ($(".tmp-clicker").length) {
        $(".tmp-clicker li").each(function() {
            $(this).click(function(e) {
                $(".tmp-clicker li").each(function() {
                    $(this).removeClass('active');
                });
                let sel = $(this).index();
                $('body').children().each(function() {
                    if ($(this).index() > 3) {
                        $(this).hide(); // hide everything
                    }
                });
                console.log($('body').children().length);
                console.log(sel + 2);
                $('body').children().eq(sel + 4).show(); // show the clicked index
                $(this).addClass('active');
            });
        });
    }
    // -----------------------------------------------------------------------

    // Vertical tabs on purchase process
    initVerticalTabs();

    // handle resizes
    $(window).resize(function() {
        $(".megamenu").height($(window).height());
        resetProgressBar(); // rest the dimensions of the progress bar
		setSameHeightAll('.same-height'); // Run same heights again
		setSameHeightAll('.same-height-1');
		setSameHeightAll('.same-height-2');
		setSameHeightAll('.same-height-3');
		setSameHeightAll('.same-height-4');
		setSameHeightAll('.same-height-5');
		setSameHeightFaq('.faq-same-height');
		equalheight('.same-height-auto');

		if ($(window).width() > 767) {
			equalheight('.same-height-nav');
			setTimeout(equalheight('.same-height-nav-col'), 100);
		};

		if ($(window).width() <= 1023) {
			if ($('.player.ytpVid').length === 1){
				$('.player.ytpVid').each(function () {
					$(this).YTPStop();
					$(this).hide();
				});
			};
		};
	});

	$('.nav.navbar-nav > li > a').click(function () {
		setTimeout(function () {
			$(window).resize();
		}, 100)
	});

    $(window).scroll(function(event) {
        didScroll = true;
    });
    // An interval here to show/hide navs on scroll
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    // set all sticky-tabs' data property to navbar height
    $(".sticky-tabs").attr("data-offset-top", navbarHeight);

    // stop the dropdown menus from closing when you click in them
    $('.dropdown-menu').click(function(e) {
        e.stopPropagation();
    });
    // Tooltips
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'hover'
	});

	$('#tabSelect').on('change', function (e) {
		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		if ($('.vertical-tabs').length) {
			$('.vertical-tabs .nav-tabs').find('a').each(function () {
				if ($(this).attr('href') === valueSelected) {
					var x = $(this).parent().index();
					$(this).trigger('click');
				};
			});
		};
	});

	$('a.promoTarger[href^="#"]').on('click', function (e) {
		var target = $(this.getAttribute('href'));
		if (target.length) {
			e.preventDefault();
			$('html, body').stop().animate({
				scrollTop: target.offset().top
			}, 1000);
		}
	});

    $(window).resize(); // trigger a resize to make sure everything is where it's supposed to be
});